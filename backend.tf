terraform {
  backend "s3" {
    bucket = "gitlabs3-bucket"
    key    = "state"
    region = "ap-southeast-1"
  }
}

